<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//access api to users
Route::group([],function(){
    Route::post('/login',[AuthController::class,'login']);
    Route::post('/logout',[AuthController::class,'logout'])->middleware('auth:api');
    Route::get('/user',[AuthController::class,'user'])->middleware('auth:api');
});
//api data users
    Route::group(['prefix'=>'users'],function(){
    Route::get('/',[UserController::class, 'index'])->middleware('auth:api');
    Route::post('/',[UserController::class, 'store'])->middleware('auth:api');
    Route::put('/{id}',[UserController::class, 'update'])->middleware('auth:api');
    Route::get('/{id}',[UserController::class, 'show'])->middleware('auth:api');
});
    //api articles
    Route::group(['prefix'=>'articles'],function(){
    Route::get('/',[ArticleController::class, 'index'])->middleware('auth:api');
    Route::post('/',[ArticleController::class, 'store'])->middleware('auth:api');
    Route::get('/{id}',[ArticleController::class, 'show'])->middleware('auth:api');
});


