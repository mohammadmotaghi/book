<?php

namespace App\Exceptions;

use Dotenv\Exception\ValidationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }


    public function render( $request , Throwable $e )
    {
     if($request->wantsJson())
     {
         /** @var ValidationException $e */
         if($e instanceof ValidationException){
            return   $this->renderValidationException($request,$e);
         }
         if($e instanceof AuthenticationException){
             return   $this->renderAuthenticationException($request,$e);
         }
         return $this->renderOtherException($request,$e);
     }
     return  parent::render($request,$e);
    }


    /**
     * @param           $request
     * @param Throwable $e
     */
    private function renderValidationException( $request , Throwable $e )
    {//controll debug of validations
       return response( ['errors' => $e->getMessage()] , 422 );
    }


    /**
     * @param Request   $request
     * @param Throwable $e
     * @return Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    private function renderOtherException( Request $request , Throwable $e )
    {
        $e   =$this->prepareException( $e);
        $code =method_exists($e,'getStatusCode') ? $e->getStatusCode:500;
       $message = 'خطای در سرور رخ داده است';
       if(!($code ==500 || empty($e->getMessage()))){
       $message =$e->getMessage();
       }
        return  response([
                             'message' =>$message
                         ], $code);
    }


      function renderAuthenticationException( Request $request , AuthenticationException $e )
       {
           return response(['errors'=>'شما به این api دسترسی ندارید'],401);
          }


}
