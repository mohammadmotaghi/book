<?php

namespace App\Http\Controllers;

use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(LoginRequest $request)
    {

      auth()->attempt([
                'email'=>$request->username,
                'password'=>$request->password,]);
                  if(auth()->check()){
                      return response(['token'=> auth()->user()->generateToken()
                           ]);

       }
       return response(['error'=>'اطلاعات کاربری اشتباه است'],401);
    }
    public function logout()
    {
            $user = auth()->guard('api')->user();
            $user->logout();
            return $user;
    }
    public function user() : ?Authenticatable
    {
        return auth('api')->user();
    }
}
