<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Http\Resources\UserCollectionResource;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    function index( Request $request )
    {
        $users= User::all();
     //   $users =UserResource::collection($users);
        $users =new UserCollectionResource($users);
        return $users;

    }


    public function show( Request $request , $id ) : UserResource
    {
         $user = User::findOrFail( $id );
         $user =new UserResource($user);
         return $user;
    }


    public function store(CreateRequest $request)
    {
        $user =User::create(['name'=>$request->name,
                                'email'=>$request->email,
                                'password' =>bcrypt($request->password)
                                ]);
        return response($user,200);
    }


    public function update(UpdateUserRequest $request,$userId)
    {
        $user =User::findOrFail($userId);
        $data =$request->only(['name','password']);
        $user->update($data);
        return response($user,202);
    }
}
