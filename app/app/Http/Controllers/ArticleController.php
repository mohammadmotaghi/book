<?php namespace App\Http\Controllers;

use App\Http\Requests\User\CreateArticlesRequest;
use App\Models\Article;
use http\Client\Curl\User;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(Request $request){
          return Article::all();
    }
    public function show( Request $request , $id )
    {
       return Article::findOrFail( $id );
    }
    public function store(CreateArticlesRequest $request)
    {
    $data =$request->only(['title','body']);
    $user =User::find(7);
    $articles =$user->articles()->create($data);
    return $articles;
    }
}
