<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollectionResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public function with($request) : array
    {
        return [
            'status'=>'ok',
            'data'=>['name'=>'mohammad'],
            'relations' =>
                [
                    'articles' => $this->articles
                ]
        ];
    }
}
