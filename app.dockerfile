FROM php:7.4-fpm

WORKDIR /var/www/server

USER root


RUN apt-get update && apt-get install -y \
    zlib1g-dev \
    libzip-dev \
    libpng-dev \
    libjpeg* \
    libfreetype6-dev \
    unzip \
    supervisor\
    && pecl install mongodb \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd


RUN docker-php-ext-install pdo pdo_mysql mysqli zip json

RUN docker-php-ext-install sockets

RUN docker-php-ext-install bcmath
# RUN pecl install grpc && \
# docker-php-ext-enable grpc

RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  echo "extension=redis.so" > /usr/local/etc/php/conf.d/redis.ini
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/mongodb.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY ./app /var/www/server
#COPY ./app/.env.staging /var/www/server/.env

RUN chmod -R 777 storage/
#RUN chmod -R 777 bootstrap/cache

COPY ./supervisord /etc/supervisor/conf.d

CMD php-fpm

